---
layout: post
title: Once Upon a Time in Hollywood Review
category: Film
permalink: /:title/
---
## SPOILERS AHEAD 

{% responsive_image path: assets/images/hollywood.jpg %}

I recently watched Quentin Tarentino’s latest film, Once Upon a Time in Hollywood. And after some time speculating, and  ‘analyzing’, I can finally formalize my strings of thoughts into (hopefully) one coherent post! So, was the film everything I wanted and more from my favourite director? Well.. No, but that doesn’t mean that I didn’t enjoy it. I always find it fascinating the way Tarentino can have multiple subplots and layers, and still somehow manage to bring it all back in the conclusion. I suppose that is a director’s lifelong dream, or something like that.

It actually wasn’t until after the film that I made the connection to pretty much everything *gasp*, and in that moment it all made sense. A lot of the small details or dialogues started to make sense and I could settle with the story, and focus more on the film. Creating the two fictional, yet very realistic characters with Brad Pitt, and Leonardo Dicaprio, made for an entertaining main storyline that kept the draw as the subplots converged. 

My favourite aspect of the film though, was hands down Brad Pitt. The character choice for him was perfect I thought. The nonchalaunt nature, coupled with the seemingly dangerous backstory about his character killing his wife gave me an element of uncertainty. And that uncertainty was met with a thunderous strike at the conclusion of the film. 

Of course I got all that I came to expect with a quintessential Tarentino film, the slow paced, dialogue heavy, ultra-violent style we know and love with a little extra bit of character depth. All packaged up in the midst of one of the biggest stories to come out of Hollywood.
