---
layout: page
title: About Me
permalink: /about/
---

Hi, I'm Aidan. 

I'm from Nanaimo, British Columbia, Canada.

I'm currently a final year University student studying Information Technology & Applied Systems, interested in Cyber Security/privacy, open source software, *NIX, Cloud/micro services, and I love good technical documentation.

Aside from technology I also enjoy film, beer, hiking, and travel. 

This site is for posting my thoughts, guides/walkthroughs, and other tech-related subjects.

You can view my resume [here](https://docs.google.com/document/d/1Gw1rOr6srF1ZgoRTJrZ9dRuKd3fcLWACwG96c6ONce8/edit?usp=sharing), and you can email me <a href="mailto:aidanbrownj@gmail.com">here</a>.

{% responsive_image path: assets/images/tressel1.jpg %}
